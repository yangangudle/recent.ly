export const TRENDLOCATION = {
    RIGHT: 'right',
    LEFT: 'left',
    TOP: 'top',
    BOTTOM: 'bottom'
}

export const TRENDTYPES = {
    TOP: 'Top',
    RECENT: 'RECENT'
}
