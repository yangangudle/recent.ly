import React from 'react';
import ReactDOM from 'react-dom';
import CssBaseline from '@material-ui/core/CssBaseline';
import { AppProvider } from './context/AppContext';
import { UserSettingsProvider } from './context/UserSettingsContext';
import App from './App';

ReactDOM.render(
  <React.StrictMode>
    <CssBaseline />
    <AppProvider>
      <UserSettingsProvider>
        <App />
      </UserSettingsProvider>
    </AppProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// serviceWorker.unregister();
