import React, { createContext, useState, useContext } from 'react';

const AppContext = createContext(undefined);

const AppProvider = ({ children }) => { 
    const [ url, setUrl ] = useState('https://www.youtube.com/embed/7IB44z_4lfk');
    const [ error ] = useState(null);

    const changeUrl = (url) => {
        // Verify that the users has entered a valid url otherwise add to error
        if(url.includes("youtube.com")) {
            url = url.replace("watch?v=", "embed/")
            setUrl(url);
            return  true;
        }
        setUrl(url);
    }
    
    return (
        <AppContext.Provider value={{
            url,
            error,
            changeUrl
        }}>
            {children}
        </AppContext.Provider>
    )
}

/*
* If you're unable to call useContext inside the component it's probably not wrapped inside a provider so throw error
*/
const useApp = () => {
    const context = useContext(AppContext);
    if(context === undefined) {
        throw new Error("useApp can only be used inside AppProvider")
    }

    return context;
}

export {
    AppProvider,
    useApp
}