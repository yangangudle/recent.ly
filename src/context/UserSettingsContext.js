import React, { createContext, useState, useContext } from 'react';
import { TRENDLOCATION, TRENDTYPES  } from '../constants';

const UserSettingsContext = createContext(undefined);

const UserSettingsProvider = ({ children }) => {
    const [ showTrends, setShowTrends] = useState(false); 
    const [ trendingTopics, setTrendingTopics ] = useState(['breakingnews']);
    const [ trendShowLocation, setTrendShowLocation ] = useState(TRENDLOCATION.RIGHT);
    const [ panelSize, setPanelSize ] = useState(20); 
    const [ panelOpacity, setPanelOpacity ] = useState(100);
    const [ trendType, setTrendType ] = useState(TRENDTYPES.TOP); 

    const toggleShowTrends = () => {
        setShowTrends(!showTrends);
    }

    const addTrendingTopic = (topic) => {
        setTrendingTopics([...trendingTopics, topic ]);
    }

    const _removeFromArr = (items, item) => {
        return items.filter( i => i !== item)
    }

    const removeTrendingTopic = (topic) => {
        setTrendingTopics(_removeFromArr(trendingTopics, topic))
    }

    
    return (
        <UserSettingsContext.Provider value={{
            showTrends, 
            toggleShowTrends, 
            trendingTopics,
            addTrendingTopic,
            removeTrendingTopic,
            trendShowLocation, 
            setTrendShowLocation,
            trendType,
            setTrendType,
            panelSize,
            setPanelSize,
            panelOpacity, 
            setPanelOpacity,
            TRENDLOCATION
        }}>
            {children}
        </UserSettingsContext.Provider>
    )
}

/*
* If you're unable to call useContext inside the component it's probably not wrapped inside a provider so throw error
*/
const useSettings = () => {
    const context = useContext(UserSettingsContext);
    if(context === undefined) {
        throw new Error("useSettings can only be used inside UserSettingsProvider")
    }

    return context;
}

export {
    UserSettingsProvider,
    useSettings
}