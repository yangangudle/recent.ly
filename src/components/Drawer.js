import React, { useState } from 'react';
import MenuIcon from '@material-ui/icons/Menu';
import Twitter from '@material-ui/icons/Twitter';
import IconButton from '@material-ui/core/IconButton';
import { makeStyles } from '@material-ui/core/styles';
import { 
    Paper as MUPaper,
    Chip,  
    Drawer, 
    Typography, 
    Divider,
    Grid,  
    TextField,
    FormControlLabel,
    Button
} from '@material-ui/core';
import styled from 'styled-components';

import { useSettings } from '../context/UserSettingsContext';
import { TRENDLOCATION, TRENDTYPES  } from '../constants';
import IOSSwitch from './IosSwitch';
import CustomizedSlider from './Slider';

const Paper = styled(MUPaper)`
    width: 30vw;
    height: 100%;
    padding: 20px;
`;

const useStyles = makeStyles((theme) => ({
    menuButton: {
        marginRight: theme.spacing(2),
    },
    button: {
        marginRight: '10px', 
        marginBottom: '10px'
    }
}));


const SideBar = () => {
    const classes = useStyles();
    const [ isOpen, setIsOpen ] = useState(false);
    const [ currentInput, setCurrentInput ] = useState('');
    const settings = useSettings();

    return (
        <>
            {
                isOpen && (
                    <Drawer 
                       
                        anchor={
                            settings.trendShowLocation === TRENDLOCATION.RIGHT ?  TRENDLOCATION.LEFT : TRENDLOCATION.RIGHT
                        }  
                        open={isOpen} onClose={() => setIsOpen(false)}
                        BackdropProps={{ invisible: true }}
                    >
                        <Paper className={classes.drawer} 
                            style={
                                settings.trendShowLocation === TRENDLOCATION.RIGHT ? { right: "10%" } : null
                            }
                        >
                            
                            <Typography variant="h6" component="h6" >
                                Your Settings
                            </Typography>
                            <Divider />
                            <Grid>
                                <br />
                                <br />
                                <FormControlLabel
                                    control={<IOSSwitch checked={settings.showTrends} onChange={() => settings.toggleShowTrends()} name="checkedB" />}
                                    label="Show Twitter Trends"
                                />
                            </Grid>
                            <Grid>
                                <br />
                                <br />
                                <TextField
                                    label="Add A Topic"
                                    id="add a topic"
                                    placeholder="Add Topic"
                                    variant="outlined"
                                    value={currentInput}
                                    onKeyUp={(e) => {
                                        // ToDo: Move this out into it's own function
                                        if(e.keyCode === 13) {
                                            if(!currentInput) {
                                                return false
                                            }

                                            settings.addTrendingTopic(currentInput);
                                            setCurrentInput('');
                                        }
                                    }}
                                    onChange={(e) => setCurrentInput(e.target.value)}
                                />
                            </Grid>
                            <Grid>
                                <br />
                                <br />
                                {
                                    settings && settings.trendingTopics.map((item) => (
                                        <Chip
                                            className={classes.button}
                                            variant="outlined"
                                            size="medium"
                                            icon={<Twitter />}
                                            label={item}
                                            onDelete={() => settings.removeTrendingTopic(item)}
                                            color="primary"
                                        />
                                    ))
                                }
                            </Grid>
                            <Grid>
                                <br />
                                {
                                    Object.entries(TRENDLOCATION).map((item) => (
                                        <Button 
                                            onClick={() => settings.setTrendShowLocation(item[1])}
                                            className={classes.button}
                                            variant={settings.trendShowLocation === item[1] ? "contained" : "outlined" } 
                                            color="primary">
                                            {item[1]}
                                        </Button>
                                    ))
                                }
                            </Grid>
                            <Grid>
                                <br />
                                <Typography variant="h6" component="h6" >
                                    Customize Panel
                                </Typography>
                                <Divider />
                                <br />
                                <br />
                                {
                                    Object.entries(TRENDTYPES).map((item) => (
                                        <Button
                                            onClick={() => settings.setTrendType(item[1])}
                                            className={classes.button}
                                            variant={settings.trendType === item[1] ? "contained" : "outlined"  }
                                            color="primary"
                                        >
                                            {item[1]}
                                        </Button>
                                    ))
                                }
                            </Grid>
                            <Grid>
                                <br />
                                <FormControlLabel 
                                    labelPlacement="top"
                                    label="Panel Size"
                                    control={<CustomizedSlider value={settings.panelSize} onChange={settings.setPanelSize} />}
                                />
                            </Grid>
                            <Grid>
                                <br />
                                <FormControlLabel 
                                    labelPlacement="top"
                                    label="Panel Opacity"
                                    control={<CustomizedSlider 
                                        value={settings.panelOpacity}
                                        onChange={settings.setPanelOpacity}
                                    />}
                                />
                            </Grid>
                        </Paper>
                    </Drawer>
                )
            }
            <IconButton
                onClick={() => setIsOpen(!isOpen)}
                edge="start"
                className={classes.menuButton}
                color="inherit"
                aria-label="open drawer"
            >
                <MenuIcon />
            </IconButton>
        </>
    )
}

export default SideBar;