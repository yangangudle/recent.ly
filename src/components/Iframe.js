import React from 'react';
import styled from 'styled-components';

const IFrame = styled.iframe`
    width: 100vw;
    height: 100vh;
    border: 0;
    overflow-y: hidden;
    overflow-x: hidden;
`

const StyledIFrame = ({ url }) => {
    return (
        <IFrame title={url} src={url}>
        </IFrame>
    )
}

export default StyledIFrame;