import React from 'react';
import { Paper } from '@material-ui/core';
import { useSettings } from '../context/UserSettingsContext';
import { TRENDLOCATION } from '../constants';
import { TwitterTimelineEmbed } from 'react-twitter-embed'

const getStyle = (locationSetting,  size, opacity ) => {
    switch(locationSetting) {
        case TRENDLOCATION.BOTTOM: 
         return {
            position: 'fixed',
            bottom: '0',
            width: '100%',
            height: `${size}%`,
            borderRadius: '0',
            opacity: `${(opacity / 100 )}`
         }
        case TRENDLOCATION.TOP: 
          return {
            position: 'fixed',
            top: '0',
            width: '100%',
            height: `${size}%`,
            borderRadius: '0',
            opacity: `${(opacity / 100 )}` 
          }
        case TRENDLOCATION.LEFT:
            return {
                position: 'fixed',
                top: '65px',
                bottom: '0',
                width: `${size}%`,
                borderRadius: '0',
                opacity: `${(opacity / 100 )}`
            }
        case TRENDLOCATION.RIGHT:
            return {
                position: 'fixed',
                top: '65px',
                bottom: '0',
                right: 0,
                width: `${size}%`,
                borderRadius: '0',
                opacity: `${(opacity / 100 )}`
            }
        default: 
            return { }
    }
}

const TweetDeck = () => {
    const settings = useSettings();

    return (
        <>
            {
                settings.showTrends ? (
                    <Paper style={getStyle(settings.trendShowLocation, settings.panelSize, settings.panelOpacity)}>
                        
                        <TwitterTimelineEmbed
                            sourceType="list"
                            ownerScreenName="palafo"
                            slug="breakingnews"
                            autoHeight
                            noHeader
                            noFooter
                            noScrollbar
                        />
                    </Paper>
                ): null
            }
        </>
    )
}

export default TweetDeck;