import React, { useState } from 'react';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import InputBase from '@material-ui/core/InputBase';
import { fade, makeStyles } from '@material-ui/core/styles';
import SearchIcon from '@material-ui/icons/Search';

import SideBar from './Drawer';
import { useApp } from '../context/AppContext';

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: '#673ab7'
  },
  
  title: {
    flexGrow: 1,
    display: 'none',
    [theme.breakpoints.up('sm')]: {
      display: 'block',
    },
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    '&:hover': {
      backgroundColor: fade(theme.palette.common.white, 0.25),
    },
    marginLeft: 0,
    marginRight: 20,
    width: '100%',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(1),
      width: '50vh',
    },
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    cursor: "default"
  },
  inputRoot: {
    color: 'inherit',
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    // vertical padding + font size from searchIcon
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create('width'),
    width: '30vw',
    [theme.breakpoints.up('sm')]: {
      width: '33ch',
      '&:focus': {
        width: '33ch',
      },
    },
  },
}));

const Header = () => {
  const classes = useStyles();
  const app = useApp();
  const [ currentInput, setCurrentInput ] = useState(app.url);
  const [ isVisible, setIsVisble ] = useState(true);

  const changeUrl = (url) => {
    app.changeUrl(url)
  }
  

  return (
    <div className={classes.root} style={ isVisible ? { opacity: 1 } : { opacity: 0, height: 0 } }>
        <AppBar 
          position="static" 
          onMouseLeave={() => setIsVisble(false)}
          onMouseEnter={() => setIsVisble(true)}>
          <Toolbar>
            <SideBar />
            <Typography className={classes.title} variant="h6" noWrap>
              Recent.ly
            </Typography>
            <div className={classes.search}>
              <div className={classes.searchIcon} >
                  <SearchIcon />
              </div>
              <InputBase
                placeholder="URL"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput,
                }}
                value={currentInput}
                onKeyUp={(e) =>  {
                  if(e.keyCode === 13) {
                      changeUrl(currentInput)
                  }
                  
                }}
                inputProps={{ 'aria-label': 'URL' }}
                onChange={(e) => setCurrentInput(e.target.value)}
              />
            </div>
            <SideBar />
          </Toolbar>
        </AppBar>
    </div>
  );
}

export default Header;