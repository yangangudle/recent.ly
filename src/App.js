import React from 'react';
import styled from 'styled-components';
import { Header, IFrame, TweetDeck } from './components';
import { useApp } from './context/AppContext';

const Container = styled.div`
  position: fixed
`

const App = () => {
  const app = useApp(); 
  
  const { url } = app;

  return (
    <Container>
      <Header />
      <IFrame url={url} />
      <TweetDeck />
    </Container>
  )
}

export default App;
