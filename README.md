# RECENT.LY 
### Your shows and your social feed in one place!

Demo: http://recently-demo.surge.sh/

![Example](https://i.imgur.com/CvCf9NC.png)

Watch you favourite show, video or live stream without having to look down at your phone. Recent.ly shows puts your stream and social media feed in one place

## Set up Instructions

1. Clone the repo `git clone https://bitbucket.org/yangangudle/recent.ly.git`
2. Install dependecnies `npm install` or `yarn install`
3. Run `npm start` or `yarn start`


